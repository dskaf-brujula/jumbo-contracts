const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

// Import vendor css
gulp.task('css', () => {
    return gulp.src([
        'node_modules/bootstrap/dist/css/bootstrap.min.css',
        'node_modules/bootstrap/dist/css/bootstrap.min.css.map',
        'node_modules/jquery-ui-dist/jquery-ui.min.css'
    ])
        .pipe(gulp.dest('webapp/content/css/vendor/'));
});

// Import vendor js
gulp.task('js', () => {
    return gulp.src([
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js.map',
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery/dist/jquery.min.map',
        'node_modules/jquery-ui-dist/jquery-ui.min.js'
    ])
        .pipe(gulp.dest('webapp/content/js/vendor'))
        .pipe(browserSync.stream());
});

gulp.task('sass', () => {
    return gulp.src([
        'webapp/scss/jumbo-contracts.scss'
    ])
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(gulp.dest('webapp/content/css'))
        .pipe(browserSync.stream());
});

gulp.task('sassmap', function () {
    return gulp.src([
        'webapp/scss/jumbo-contracts.scss'
    ])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('webapp/content/css'))
});

gulp.task('fonts', () => {
    return gulp.src('node_modules/@mdi/font/fonts/*')
        .pipe(gulp.dest('webapp/content/fonts'));
});

gulp.task('serve', ['sass'], () => {
    browserSync.init({
        server: './webapp'
    });
    gulp.watch([
        'webapp/scss/*.scss',
        'webapp/scss/components/*.scss',
        'webapp/scss/custom/*.scss',
        'webapp/scss/utilities/*.scss',
        'webapp/scss/mixins/*.scss'
    ], ['sass']);
    gulp.watch('webapp/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['css', 'js', 'serve', 'sassmap', 'fonts'])