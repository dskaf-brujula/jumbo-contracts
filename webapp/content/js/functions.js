//Función Día actual
function getDate() {
    var fullDate = new Date()
    var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
    var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
    $("#headerDate").html(currentDate);
}

//Acción mostrar/ocultar Búsqueda Avanzada
$("#btnBuscadorAvanzadoClose").css('opacity','0');
$("#buscadorAvanzado").css('display','none');

$("#btnBuscadorAvanzado").click(function () {
    $(this).fadeTo("slow", 0, function () {
        $(this).hide();
        $("#btnBuscadorAvanzadoClose").fadeTo("slow", 1);
    });
    $("#buscadorAvanzado").slideDown("slow");
});

$("#btnBuscadorAvanzadoClose").click(function () {
    $(this).fadeTo("slow", 0);
    $("#buscadorAvanzado").slideUp("slow", function () {
        $("#btnBuscadorAvanzado").fadeTo("slow", 1, function () {
            $("#btnBuscadorAvanzado").show();
        });
    });
});