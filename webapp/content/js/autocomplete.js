//Función Focus para autocomplete multiselect
$("#cliente").focusin(function () {
    $(this).parent().addClass("focus");
});

$("#cliente").focusout(function () {
    $(this).parent().removeClass("focus");
});

$("#tarifa").focusin(function () {
    $(this).parent().addClass("focus");
});

$("#tarifa").focusout(function () {
    $(this).parent().removeClass("focus");
});



$(function () {
    var nombrehotel = [
        "Iberostar Suites Hotel Jardín del Sol - Adults Only",
        "Hotel Son Trobat Wellness & Spa",
        "Hotel Coronado Thalasso & Spa",
        "Bo Hotel Palma",
        "Hotel Bellver by Meliá",
        "Palladium+",
        "Gran Meliá Victoria",
        "Gomila Park",
        "HM Jaime III",
        "Born",
        "Araxa - Adults Only",
        "Eurostars Marivent",
        "Icon Rosetó",
        "Hotel Armadams",
        "Meliá Palma Bay",
        "Hotel Continental",
        "Amic Horizonte",
        "AC Hotel Ciutat de Palma",
        "Innside by Meliá Palma Bosque",
        "BQ Belvedere",
        "Catalonia Majórica",
        "Sercotel Zurbarán"
    ];
    $("#nombrehotel").autocomplete({
        source: nombrehotel
    });
});

//Variables Cadena hotelera
$(function () {
    var cadenaHotelera = [
        "Meliá Hotels International",
        "NH Hotel Group",
        "Barceló Hotel Group",
        "Riu Hotels & Resorts",
        "BlueBay Hotels",
        "Zafiro Hoteles",
        "AC Hotels by Marriott",
        "BQ Hotels",
        "Hipotels",
        "Grupo Batle",
        "THB Hotels",
        "Blue Sea Hotels",
        "Iberostar"
    ];
    $("#cadenaHotelera").autocomplete({
        source: cadenaHotelera
    });
});

//Variables Clientes
$(function () {
    var cliente = [
        "Alpitour",
        "Ávoris",
        "Barceló",
        "Eden",
        "Viaggi",
        "Funhotel",
        "Brújula",
        "CMC Group",
        "Travelplan",
        "Iberostar",
        "WelcomeBeds"
    ];
    $("#cliente").autocomplete({
        source: cliente,
        multiselect: true
    });
});

//Variables Tarifa
$(function () {
    var tarifa = [
        "CAR - Coche incluido",
        "MAL - Market ALPITOUR",
        "DYN - Tarifa Dinamica",
        "NET - Nett // Fit Rate",
        "CAR - Coche incluido",
        "MAL - Market ALPITOUR",
        "DYN - Tarifa Dinamica",
        "NET - Nett // Fit Rate",
        "MAL - Market ALPITOUR"
    ];
    $("#tarifa").autocomplete({
        source: tarifa,
        multiselect: true
    });
});

//Variables Gestor
$(function () {
    var gestor = [
        "CAR - Coche incluido",
        "MAL - Market ALPITOUR",
        "DYN - Tarifa Dinamica",
        "NET - Nett // Fit Rate",
        "CAR - Coche incluido",
        "MAL - Market ALPITOUR",
        "DYN - Tarifa Dinamica",
        "NET - Nett // Fit Rate",
        "MAL - Market ALPITOUR"
    ];
    $("#gestor").autocomplete({
        source: gestor
    });
});